layout: false
class: center middle

# How small a module should be

### (between classes and applications)

by Jakub nabrdalik

---

# Definition of a module

--

#### Single responsibility 
for a process (a verb), not data (noun)

#### Has its own database
other modules cannot read/write there, they have to talk with that API

#### A vertical slice 
all layers: from DB access to API or even UI

#### Clearly identifiable API 
you know what to call

#### Encapsulates everything
everything apart from the API is hidden and inaccessible

---

layout: false
background-image: url(img/userModule2.png)
background-size: 50%
background-repeat: no-repeat
background-position: center middle

# User module is too much


---


layout: false
background-image: url(img/userSplit2.png)
background-size: 50%
background-repeat: no-repeat
background-position: center middle

# Responsible for process, not data

---

layout: false
background-image: url(img/howsmall1.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# How small should a module be

--

.footnote[
0. get cost per user
1. create and invoice (read legal documents, learn VAT taxes)
2. generate PDF (read PDF library documentation)
3. send an email (read Apache Camel or Spring Integration docs)
]

---


layout: false
background-image: url(img/howsmall2.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# How small should a module be

.footnote[
0. get cost per user
1. create and invoice (read legal documents, learn VAT taxes)
2. generate PDF (read PDF library documentation)
3. send an email (read Apache Camel or Spring Integration docs)
]

---


layout: false
background-image: url(img/howsmall2.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# How small should a module be


.footnote[
If two steps of a process require different knowledge, and can be written by two developers working with a contract - split it into two modules

Small modules are easier to replace, refactor, debug. See [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy)
]

---

# Minimize cognitive load

It's not about how much code you have, but how much cognitive load.

The less you have to put into your head, the easier it is to implement.

That's the whole reason we have modules in the first place.

--

.footnote[

This presentation is available at https://jakubn.gitlab.io/jugacademy2022

Contact me at

email: jakubn@gmail.com

twitter: @jnabrdalik
]